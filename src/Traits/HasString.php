<?php


namespace SFExpressIsp\Traits;


use SFExpressIsp\Core\ItemInterface;

trait HasString
{
    public function __toString(): string
    {
        $string = '';
        foreach ($this->getData() as $key => $value) {
            if($this->isObjType($key, $value)){
                if($value instanceof ItemInterface){
                    $string .=  sprintf('<%s>%s</%s>', $key, $value->__toString(), $key);
                }
            }else if(is_array($value)) {
                foreach ($value as $item){
                    if($item instanceof ItemInterface){
                        $string .=  sprintf('<%s>%s</%s>', $key, $item->__toString(), $key);
                    }
                }
            }else{
                $string .= sprintf('<%s>%s</%s>', $key, $value, $key);
            }
        }
        return $string;
    }
}