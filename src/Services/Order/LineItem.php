<?php


namespace SFExpressIsp\Services\Order;


use SFExpressIsp\Core\ItemAbstract;

class LineItem extends ItemAbstract
{
    protected $objTypes = ['AdditionalDataList'];

    protected $data = [
        'LineNo' => '',
        'ItemID' => '',
        'ItemName' => '',
        'Brand' => '',
        'Specifications' => '',
        'ItemValue' => '',
        'ItemValueCurrencyCode' => '',
        'ItemQuantity' => '',
        'ItemUnit' => '',
        'ItemWeight' => '',
        'WeightUnit' => '',
        'CountryOfOrigin' => '',
        'ProductRecordNo' => '',
        'GoodPrepardNo' => '',
        'StateBarCode' => '',
        'AdditionalDataList' => null,
    ];
}