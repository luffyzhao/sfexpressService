<?php


namespace SFExpressIsp\Services\Order;


use SFExpressIsp\Core\ItemAbstract;

class AdditionalDataList  extends ItemAbstract
{
    protected $data = [
        'cargoCount' => '',
        'NetWeight' => '',
        'CBDateTime' => '',
        'Freight' => '',
        'insuredFee' => '',
        'e-commerceCode' => '',
        'cebFlag' => '',
        'GoodsOwner' => '',
        'Warehouse' => '',
        'Platform' => '',
        'HarmonizedCode' => '',
        'AESCode' => '',
        'PrintSize' => '',
        'OperateFlag' => '',
        'ReturnSign' => '',
        'VATCode' => '',
        'IsBattery' => '',
        'GoodsURLLink' => '',
        'DistrNote' => '',
        'DistrPickName' => '',
        'ERPCode' => '',
        'CustomEcommerceCode' => '',
        'PrimaryQty' => '',
        'SecondaryQty' => '',
        'SecondaryUnit' => '',
        'IDCardURLLink' => '',
    ];

    public function __toString(): string
    {
        $string = '';
        foreach ($this->getData() as $key=>$value){
            $string .= sprintf('<AdditionalData><Key>%s</Key><Value>%s</Value></AdditionalData>', $key, $value);
        }
    }
}