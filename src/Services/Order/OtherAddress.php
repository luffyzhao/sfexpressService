<?php


namespace SFExpressIsp\Services\Order;


use SFExpressIsp\Core\ItemAbstract;

class OtherAddress extends ItemAbstract
{
    use HasItem, HasData, HasString;
}