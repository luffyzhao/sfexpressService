<?php


namespace SFExpressIsp\Services\Order;


use SFExpressIsp\Core\ItemAbstract;

class TradeOrder extends ItemAbstract
{
    protected $objTypes = ['AdditionalDataList'];
    protected $data = [
        'TradeOrderNo' => '',
        'SiteName' => '',
        'OrderDate' => '',
        'BuyerName' => '',
        'BuyerCertType' => '',
        'BuyerCertNo' => '',
        'BuyerNick' => '',
        'BuyerMobile' => '',
        'BuyerPhone' => '',
        'BuyerEmail' => '',
        'SellerName' => '',
        'SellerNick' => '',
        'StoreName' => '',
        'StoreID' => '',
        'SellerMobile' => '',
        'SellerPhone' => '',
        'SellerEmail' => '',
        'PaymentTool' => '',
        'PaymentNo' => '',
        'PayDate' => '',
        'PricePaid' => '',
        'BuyerPaymentAccountNo' => '',
        'SellerPaymentAccountNo' => '',
        'AdditionalDataList' => null,
    ];

}