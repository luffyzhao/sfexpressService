<?php


namespace SFExpressIsp\Services\Order;


use SFExpressIsp\Core\ItemAbstract;


class ShipFromAddress extends ItemAbstract
{

    protected $objTypes = ['AdditionalDataList'];

    protected $data = [
        'CompanyName' => '',
        'Contact' => '',
        'Mobile' => '',
        'CountryCode' => 'CN',
        'StateOrProvince' => '',
        'City' => '',
        'County' => '',
        'AddressLine1' => '',
        'AddressLine2' => '',
        'ShipperCode' => '',
        'AdditionalDataList' => null,
    ];

}