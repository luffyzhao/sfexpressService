<?php


namespace SFExpressIsp\Services\Order;


use SFExpressIsp\Core\ItemAbstract;

class Parcels extends ItemAbstract
{
    protected $data = [
        'Length' => 0,
        'Width' => 0,
        'Height' => 0,
        'Weight' => 0,
        'Volume' => 0,
        'TempRange' => 1,
    ];
}