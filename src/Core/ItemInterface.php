<?php


namespace SFExpressIsp\Core;


interface ItemInterface
{
    public function __toString():string ;
}