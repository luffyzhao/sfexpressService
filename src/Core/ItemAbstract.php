<?php


namespace SFExpressIsp\Core;


use SFExpressIsp\Traits\HasItem;
use SFExpressIsp\Traits\HasString;

abstract class ItemAbstract implements ItemInterface
{
    use HasItem, HasString;
}